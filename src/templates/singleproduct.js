// src/templates/item.js
import React from 'react';
import { useState,useEffect } from 'react';
import '../style/product.css'
import { Link } from 'gatsby';


const Singleproduct = ({ pageContext }) => {
  const  id = pageContext;
  console.log(id.id)
  const [item, setItem] = useState(null);


  useEffect(() => {
      const headers = {
        Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0OCwicm9sZSI6ImFkbWluIiwiaWF0IjoxNzEwNDc5NTA2LCJleHAiOjc0NzA0Nzk1MDZ9.BDtEGM5HDMCfsHqDExZoLWpfQVDg6X4Zq7ZZNOT6wh8',
        method: "GET",
        withCredentials: true,
        "Content-Type": "application/json",
        "ngrok-skip-browser-warning": true,
      };
    const fetchProduct = async () => {
      try {
        await fetch(`http://localhost:9000/product/get/product/${id.id}`, { headers }).then(
          async (response) => {
            const data = await response.json();
            setItem(data);
          }
        );
      } catch (error) {
        console.error("Error fetching secret data:", error);
      }
    };
    fetchProduct();
  }, [id]);
  const addtocart = async () => {
    try {
      await fetch(`http://localhost:9000/cart/post`, {         method: "POST",
      headers: {
        Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0OCwicm9sZSI6ImFkbWluIiwiaWF0IjoxNzEwNDc5NTA2LCJleHAiOjc0NzA0Nzk1MDZ9.BDtEGM5HDMCfsHqDExZoLWpfQVDg6X4Zq7ZZNOT6wh8",
        "ngrok-skip-browser-warning": true,
        "content-type": "application/json",
      }, 
              body: JSON.stringify({
        product_id: id.id,
        quantity: Number(1),
      })}).then(
        async (response) => {
         await response.json();
          alert("Successfully added to cart");        }
      );
    } catch (error) {
      console.error("Error fetching secret data:", error);
    }
  };
  


  return (
    <div>

      {
      
      item ? (
        <div className="card my-2 h-80" >
        <img src={item.product_image} height={"300px"} className="card-img-top" alt="..."/>
        <div className="card-body">
          <h5 className="card-title">{item.product_name}...</h5>
          <p className="card-text">{item.product_description}...</p>
          <p className='card-text'>Price:-{item.product_price}</p>
          <button className='btn btn-primary' onClick={addtocart}>Add To Cart</button>
          <Link to='/cartdetails'>
          <button className='btn btn-primary'>View Cart</button></Link>


      
          
      
        </div>
      </div>
      ) : (
        <p>Loading...</p>
      )
      }
    </div>
  );
};

export default Singleproduct;
