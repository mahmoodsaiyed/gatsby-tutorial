// src/templates/item.js
import React from 'react';
import { useState,useEffect } from 'react';
import '../style/product.css'
import { loadStripe } from "@stripe/stripe-js";



const Cart = () => {

  const [item, setItem] = useState('');

  useEffect(() => {
    const headers = {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0OCwicm9sZSI6ImFkbWluIiwiaWF0IjoxNzEwNDc5NTA2LCJleHAiOjc0NzA0Nzk1MDZ9.BDtEGM5HDMCfsHqDExZoLWpfQVDg6X4Zq7ZZNOT6wh8',
      method: "GET",
      withCredentials: true,
      "Content-Type": "application/json",
      "ngrok-skip-browser-warning": true,
    };
    const fetchProduct = async () => {
      try {
        await fetch(`http://localhost:9000/cart/get`, { headers }).then(
          async (response) => {
            const data = await response.json();
            setItem(data);
          }
        );
      } catch (error) {
        console.error("Error fetching secret data:", error);
      }
    };
    fetchProduct();
  }, []);
  
  async function handledelete(cart_id, product_id) {
    console.log(cart_id, product_id);
    try {
      await fetch(
        `http://localhost:9000/cart/delete`,
        {
          method: "DELETE",
          headers: {
            Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0OCwicm9sZSI6ImFkbWluIiwiaWF0IjoxNzEwNDc5NTA2LCJleHAiOjc0NzA0Nzk1MDZ9.BDtEGM5HDMCfsHqDExZoLWpfQVDg6X4Zq7ZZNOT6wh8',
            
            "ngrok-skip-browser-warning": true,
            "content-type": "application/json",
          },
          body: JSON.stringify({
            cart_id: cart_id,
            product_id: product_id,
          }),
        }
      ).then(async (response) => {
        await response.json();
         window.location.reload(true);
      });
    } catch (error) {
      console.error("Error fetching secret data:", error);
    }   
    
  }
    // payment integration
    const makePayment = async () => {
        const stripe = await loadStripe(
          "pk_test_51OgSGjSCvPW0BRZvDI7KRUEB7cgDjApbwVOAFnPutpZ4v9JkEpttd3HkZhsOEz8s0OzPzJYs9FcRJ4KC7ABT9EyC00LC4xNYxI"
        );
    
        // const body = {
        //   total_amount: cartproduct.total_price_of_all_product,
        // };
        const headers = {
          mode: "no-cors",
          Authorization:
          'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0OCwicm9sZSI6ImFkbWluIiwiaWF0IjoxNzEwNDc5NTA2LCJleHAiOjc0NzA0Nzk1MDZ9.BDtEGM5HDMCfsHqDExZoLWpfQVDg6X4Zq7ZZNOT6wh8',
          "Content-Type": "application/json",
          "ngrok-skip-browser-warning": true,
        };
        const response = await fetch(
          `http://localhost:9000/stripe/post`,
          {
            method: "POST",
            headers: headers,
            body: JSON.stringify(),
          }
        );
    
        const session = await response.json();
        console.log(session);
    
        const result = stripe.redirectToCheckout({
          sessionId: session.id,
        });
    
        if (result.error) {
          console.log(result.error);
        }
      };

  return (
    <div className='card-container'>
      
      {
    item &&  item.product_list.map(items=>(
        <div className="card my-2 h-80" >
        <img src={items.product_image} height={"300px"} className="card-img-top" alt="..."/>
        <div className="card-body">
          <h5 className="card-title">{items.product_name}...</h5>
          <p className="card-text">{items.product_description}...</p>
          <p className='card-text'>Price:-{items.total_amount_of_product_with_quantity}</p>
          <p className="card-text">Quantity : {items.quantity}</p>
          <button
                    className="btn btn-danger"
                    onClick={() => {
                      handledelete(items.cart_id, items.product_id);
                    }}
                  >
                    Delete
                  </button>



      
          
      
        </div>
      </div>
      ))}
      <p>Total Price : {item.total_price_of_all_product}</p>
      <div>
        <button className='btn btn-primary' onClick={makePayment}>Checkout</button>
      </div>
    </div>
  );
};

export default Cart;
