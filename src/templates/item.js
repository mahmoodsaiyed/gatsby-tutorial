// src/templates/item.js
import React from 'react';
import { useState,useEffect } from 'react';
import { Link } from 'gatsby';
import '../style/product.css'


const ItemTemplate = ({ pageContext }) => {
  const  name = pageContext.name.toLowerCase(); 
  console.log(name)
  const [item, setItem] = useState('');
  console.log(item)

  useEffect(() => {
    const headers = {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0OCwicm9sZSI6ImFkbWluIiwiaWF0IjoxNzEwNDc5NTA2LCJleHAiOjc0NzA0Nzk1MDZ9.BDtEGM5HDMCfsHqDExZoLWpfQVDg6X4Zq7ZZNOT6wh8',
      method: "GET",
      withCredentials: true,
      "Content-Type": "application/json",
      "ngrok-skip-browser-warning": true,
    };
    const fetchProduct = async () => {
      try {
        await fetch(`http://localhost:9000/product/get/category/${name}`, { headers }).then(
          async (response) => {
            const data = await response.json();
            setItem(data);
          }
        );
      } catch (error) {
        console.error("Error fetching secret data:", error);
      }
    };
    fetchProduct();
  }, [name]);

  return (
    <div className='card-container'>
      
      {
    item &&  item.map(items=>(
        <div className="card my-2 h-80" >
        <img src={items.product_image} height={"300px"} className="card-img-top" alt="..."/>
        <div className="card-body">
          <h5 className="card-title">{items.product_name}...</h5>
          <p className="card-text">{items.product_description}...</p>
          <p className='card-text'>Price:-{items.product_price}</p>
          <Link to={`/product/get/product/${items.product_id}`}>

<button className='btn btn-primary' >View Product</button>
</Link>

      
          
      
        </div>
      </div>
      ))}
    </div>
  );
};

export default ItemTemplate;
