// src/pages/index.js
import React, { useState, useEffect } from 'react';
import { Link } from 'gatsby';
import '../style/product.css'

const IndexPage = () => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    const fetchdata = async () => {
      const headers = {
        Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0OCwicm9sZSI6ImFkbWluIiwiaWF0IjoxNzEwNDc5NTA2LCJleHAiOjc0NzA0Nzk1MDZ9.BDtEGM5HDMCfsHqDExZoLWpfQVDg6X4Zq7ZZNOT6wh8 `,
        method: "GET",
        withCredentials: true,
        "Content-Type": "application/json",
        "ngrok-skip-browser-warning": true,
      };
      try {
        await fetch(
          `http://localhost:9000/category/get`,
          { headers }
        ).then(async (response) => {
          const data = await response.json();
           console.log(data);
          setItems(data);
        });
      } catch (error) {
        console.error("Error fetching secret data:", error);
      }
    };
    fetchdata();

  }, []);

  return (
    <div>
      <h1>Items from MySQL Database</h1>
      {/* <ul>
          <li key={item.product_id}>
            <Link to={`/product/get/product/${item.product_id}`}>
              <strong>{item.product_name}</strong>
            </Link>
          </li>
      </ul> */}
      <div>
        
        {items.map(item => (

        <div className="card my-2 h-80" >
  <img src={item.category_image} height={"300px"} className="card-img-top" alt="..."/>
  <div className="card-body">
    <h5 className="card-title">{item.category_name}...</h5>
    <p className="card-text">{item.category_description}...</p>
    <Link to={`/product/get/category/${item.category_name.toLowerCase()}`}>

    <button className='btn btn-primary' >View Product</button>
    </Link>

    

  </div>
</div>
))}

      </div>
    </div>
  );
};

export default IndexPage;
