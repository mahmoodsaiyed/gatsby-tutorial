// gatsby-node.js
const axios = require('axios');
const path = require('path');

exports.createPages = async ({ actions }) => {
  const { createPage } = actions;
  
  // Fetch items from API

  const headers = {
    Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0OCwicm9sZSI6ImFkbWluIiwiaWF0IjoxNzEwNDc5NTA2LCJleHAiOjc0NzA0Nzk1MDZ9.BDtEGM5HDMCfsHqDExZoLWpfQVDg6X4Zq7ZZNOT6wh8`,
    withCredentials: true,
    'Content-Type': 'application/json',
    'ngrok-skip-browser-warning': true,
  };
  
    // const response = await axios.get('http://localhost:9000/product/get/category/sport', {
    //   headers: headers
    // });
    // const items = response.data;
    // console.log(items)
    

    try {
        await fetch(
          `http://localhost:9000/category/get`,
          { headers }
        ).then(async (response) => {
          const items = await response.json();
          await items.forEach(async item => {
            createPage({
              path: `/product/get/category/${item.category_name.toLowerCase()}`,
              component: path.resolve('./src/templates/item.js'),
              context: {
                name: item.category_name,
              },
            });
            await fetch(
              `http://localhost:9000/product/get/category/${item.category_name.toLowerCase()}`,
              { headers }
            ).then(async (response) => {
              const items = await response.json();
              await items.forEach( item => {
                createPage({
                  path: `/product/get/product/${item.product_id}`,
                  component: path.resolve('./src/templates/singleproduct.js'),
                  context: {
                    id: item.product_id,
                  },
                });
    
    
                
              }
              );
            });

            
          }
          );
        });
        // Create dynamic pages for each item
      } catch (error) {
        console.error("Error fetching secret data:", error);
      }

      


            createPage({
              path: `/cartdetails`,
              component: path.resolve('./src/templates/cart.js'),

            });


            

};
